import datetime

import discord
from iservscrapping import Iserv

import settings

client = discord.Client()

iserv = Iserv(settings.iserv_url, settings.user, settings.password)


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))
    game = discord.Game("in der Schule | hilfe")
    await client.change_presence(activity=game)


@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if message.content.startswith("hilfe"):
        embedhelp = discord.Embed(title="Befehle:", description="", color=0x00ff00)
        embedhelp.add_field(name="plan", value="zeigt den Stundenplan für den nächsten Schultag", inline=False)
        embedhelp.add_field(name="vertreter", value="Schickt einen Link zum Vertretungsplan", inline=False)
        await message.channel.send(embed=embedhelp)

    if message.content.startswith('vertreter'):
        embed = discord.Embed(title="Dein Vertretungsplan für Morgen:", description="", color=0x00ff00)
        plan = iserv.get_untis_substitution_plan(
            "/iserv/plan/show/raw/02-Vertreter%20Sch%C3%BCler%20morgen/subst_002.htm", settings.schoolclass)
        text = ""
        for i in plan:
            title = "hi"
            for e in i:

                if e[0] in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]:
                    title = e
                else:
                    text = text + e + "\n"
            embed.add_field(name=title, value=text, inline=False)
            text = ""

        await message.channel.send(embed=embed)
        print(datetime.datetime.today().weekday())

    if message.content.startswith('plan') or message.content.startswith('Plan'):
        embed = discord.Embed(title="Dein Stundenplan für Morgen:", description="", color=0x00ff00)
        if datetime.datetime.today().weekday() == 5:  # Samstag
            embed.add_field(name="1/2", value=settings.montag1, inline=False)
            embed.add_field(name="3/4", value=settings.montag2, inline=False)
            embed.add_field(name="3/4", value=settings.montag3, inline=False)
            await message.channel.send(embed=embed)

        if datetime.datetime.today().weekday() == 6:  # Sonntag
            embed.add_field(name="1/2", value=settings.montag1, inline=False)
            embed.add_field(name="3/4", value=settings.montag2, inline=False)
            embed.add_field(name="3/4", value=settings.montag3, inline=False)
            await message.channel.send(embed=embed)

        if datetime.datetime.today().weekday() == 0:  # Montag
            embed.add_field(name="1/2", value=settings.dienstag1, inline=False)
            embed.add_field(name="3/4", value=settings.dienstag2, inline=False)
            embed.add_field(name="3/4", value=settings.dienstag3, inline=False)
            await message.channel.send(embed=embed)

        if datetime.datetime.today().weekday() == 1:  # Dienstag
            embed.add_field(name="1/2", value=settings.mittwoch1, inline=False)
            embed.add_field(name="3/4", value=settings.mittwoch2, inline=False)
            embed.add_field(name="3/4", value=settings.mittwoch3, inline=False)
            await message.channel.send(embed=embed)

        if datetime.datetime.today().weekday() == 2:  # Mittwoch
            embed.add_field(name="1/2", value=settings.donnerstag1, inline=False)
            embed.add_field(name="3/4", value=settings.donnerstag2, inline=False)
            embed.add_field(name="3/4", value=settings.donnerstag3, inline=False)
            await message.channel.send(embed=embed)

        if datetime.datetime.today().weekday() == 3:  # Donnerstag
            embed.add_field(name="1/2", value=settings.freitag1, inline=False)
            embed.add_field(name="3/4", value=settings.freitag2, inline=False)
            embed.add_field(name="3/4", value=settings.freitag3, inline=False)
            await message.channel.send(embed=embed)

        if datetime.datetime.today().weekday() == 4:  # Freitag
            embed.add_field(name="1/2", value=settings.montag1, inline=False)
            embed.add_field(name="3/4", value=settings.montag2, inline=False)
            embed.add_field(name="3/4", value=settings.montag3, inline=False)
            await message.channel.send(embed=embed)


client.run(settings.token)
